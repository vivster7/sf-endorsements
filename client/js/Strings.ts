/**
 * Direct copy of code from array-join-conjunction
 * https://github.com/dustinspecker/array-join-conjunction/blob/master/src/index.js
 */
export function arrayJoinConjunction(
  array: string[],
  conjunction: string = 'and',
) {
  if (!array.length) {
    return '';
  }

  if (array.length === 1) {
    return array[0];
  }

  if (array.length === 2) {
    return `${array[0]} ${conjunction} ${array[1]}`;
  }

  return `${array.slice(0, array.length - 1).join(', ')}, ${conjunction} ${
    array[array.length - 1]
  }`;
}

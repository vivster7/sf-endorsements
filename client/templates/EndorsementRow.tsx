import React from 'react';
import { UncontrolledPopover, PopoverHeader, PopoverBody } from 'reactstrap';
import { RaceMetadata, Organization } from '../../common/types';
import EndorsementDisplay from './EndorsementDisplay';

interface Props {
  /** ID suffix to add to Popover DOM IDs */
  tableId: string;
  organization: Organization;
  races: RaceMetadata[];

  /** Which key in the `organization` to use to link to the endorsements */
  linkKey: 'link' | 'propositionsLink';

  /** Map of candidate name to color, for easy browsing */
  colorMap: { [name: string]: string };
}

interface State {
  showOverlay: boolean;
}

/**
 * A single row in an endorsement table, representing the endorsements
 * of one organization
 */
export default class EndorsementRow extends React.PureComponent<Props, State> {
  state: State = { showOverlay: false };

  render() {
    const { organization, races, tableId, linkKey, colorMap } = this.props;
    const buttonId = `EndorsementRow__${organization.name.replace(
      /\W+/g,
      '',
    )}_${tableId}`;

    return (
      <tr key={organization.name}>
        <td>
          <UncontrolledPopover
            trigger="legacy"
            placement="bottom"
            target={buttonId}
            fade
          >
            <PopoverHeader>{organization.name}</PopoverHeader>
            <PopoverBody>
              <div>{organization.description}</div>
              <div className="mt-2">
                <a
                  href={
                    linkKey === 'link'
                      ? organization.link
                      : organization.propositionsLink
                  }
                  className="btn btn-primary"
                  target="_blank"
                  rel="nofollow"
                >
                  Go to endorsements source
                </a>
              </div>
            </PopoverBody>
          </UncontrolledPopover>
          <button
            className="btn btn-link p-0 font-weight-bold text-left"
            type="button"
            id={buttonId}
          >
            {organization.name}
          </button>
        </td>
        {races.map(race => {
          const endorsement = organization.endorsements[race.race];
          return (
            <td key={race.race}>
              {endorsement && (
                <EndorsementDisplay
                  endorsement={endorsement}
                  colorMap={colorMap}
                />
              )}
            </td>
          );
        })}
      </tr>
    );
  }
}
